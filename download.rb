#!/usr/bin/env ruby

require 'json'
require 'curb'

DEFAULT_LOCATION = File.expand_path("~/Downloads/backgrounds")
BACKGROUND_FILE = "https://raw.githubusercontent.com/dconnolly/chromecast-backgrounds/master/backgrounds.json"

def download(downloader, url, dest_dir)
	downloader.url = url
	filename = url.split(/\?/).first.split(/\//).last
	print "'#{url}' :"
	File.open(dest_dir + "/" + filename, 'wb') do|f|
		downloader.on_progress {|dl_total, dl_now, ul_total, ul_now| print "="; true }
		downloader.on_body {|data| f << data; data.size }   
		downloader.perform
		puts "=> '#{filename}'"
	end
end

background_list_file = ARGV[0]
background_list_file = BACKGROUND_FILE if background_list_file.nil?

download_location = ARGV[1]
download_location = DEFAULT_LOCATION if download_location.nil?

easy = Curl::Easy.new
easy.follow_location = true

download(easy, background_list_file, download_location)
filename = background_list_file.split(/\?/).first.split(/\//).last

background_list = JSON.parse(File.read(download_location + '/' + filename))


background_list.each do |item|
	download(easy, item["url"], download_location)
end