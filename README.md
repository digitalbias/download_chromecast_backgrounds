# Download Chromecast Backgrounds

There is a [github project](https://github.com/dconnolly/Chromecast-Backgrounds) which updates, periodically, the backgrounds displayed on the Google chromecast. I wanted these backgrounds on my desktop, so this script does just that.

## Requirements

* Ruby
* Bundler
* libcurl

To get this running on a Mac

* Clone the repo 
* `brew install curl`
* `bundle`
* `ruby download.rb`. This will download the json file from the dconnolly project and then download all the images listed in that file to `~/Downloads/backgrounds`


